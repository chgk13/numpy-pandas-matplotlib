import numpy as np
import pandas as pd
import scipy.stats as stat
import time
from matplotlib import pyplot as plt


def collecting_data(
        number_of_experements=200,
        number_of_tests=100,
        matrix_size=10,
        step=10,
        max_number=100):
    """
        Возвращает словарь, содержащий данные об измерениях времени
        перемножения квадратных целочисленных матриц .

        number_of_experements - количество различных размеров матриц

        number_of_tests - количество измерений времени для матриц одного
        размера

        matrix_size - начальный размер матрицы

        step - шаг, на который увеличивается размер матрицы

        max_number - ограничение на максимальных элемент в матрице
    """

    result = {}

    for _ in range(number_of_experements):
        result[matrix_size] = []
        for test_index in range(number_of_tests):
            m_1 = np.random.randint(0, max_number, size=(
                matrix_size, matrix_size), dtype='int16')
            m_2 = np.random.randint(0, max_number, size=(
                matrix_size, matrix_size), dtype='int16')
            start = time.time()
            m_1 * m_2
            res_time = time.time() - start
            result[matrix_size].append(res_time)
        matrix_size += step
    return result


def create_df(data, file_name=None):
    """
        Возвращает датафрейм со статистикой.
        Если указано имя файла, сохраняет туда датафрейм с сырыми
        данными в формате csv.
    """

    result_df = pd.DataFrame(data)
    if file_name:
        result_df.to_csv(file_name)
    return result_df.describe().iloc[1:3]


def df_from_csv_file(file_name):
    raw_df = pd.read_csv(file_name, index_col=0)
    return raw_df.describe().iloc[1:3]


def compute_n(df):
    time = np.array(df.iloc[0])
    size = np.array(df.columns, dtype='float')
    n = (np.log(time[1:] / time[:len(time)-1])) / \
        (np.log(size[1:] / size[:len(size)-1]))
    return n


def create_plot(df):
    x = list(map(int, df.columns))
    y = df.iloc[0]
    ax = plt.subplot()
    ax.errorbar(x[::7], y[::7], yerr=df.iloc[1][::7], fmt='.')

    plt.title('Matrix multiplication')
    plt.xlabel('matrix_size')
    plt.ylabel('avg_time')
    plt.plot(x, y)
    plt.savefig('plot_1.png')
    plt.savefig('plot_1.eps')
    plt.show()


def create_log_plot(df):
    x = np.log(list(map(int, df.columns)))
    y = np.log(df.iloc[0])
    reg = stat.linregress(x=x, y=y)
    yerr = np.log(df.iloc[0] + df.iloc[1]) - y
    ax = plt.subplot()
    ax.errorbar(x[::7], y[::7], yerr=yerr[::7], fmt='.')

    plt.title('Matrix multiplication(log)')
    plt.xlabel('log(matrix_size)')
    plt.ylabel('log(avg_time)')
    plt.plot(x, y, label='T(N)')
    plt.plot(x, reg[1] + reg[0]*x, label=fr'$tg(\alpha) = {reg[0]}$')
    plt.legend(loc='upper left', shadow=True, fontsize='small')
    plt.savefig('plot_2.png')
    plt.savefig('plot_2.eps')
    plt.show()


def n_vs_N_plot(df):
    n = compute_n(df)
    x = list(map(int, df.columns))
    x = x[:len(x)-1]
    plt.title('n vs N')
    plt.xlabel('N')
    plt.ylabel('n')
    plt.plot(x, n, label='n(N)')
    plt.plot(x, n.mean() * np.array([1]*len(x)),
             '--', label=f'mean={n.mean()}')
    plt.legend(loc='upper left', shadow=True, fontsize='small')
    plt.savefig('plot_3.png')
    plt.savefig('plot_3.eps')
    plt.show()


def main():
    # data = collecting_data()
    # df = create_df(data)
    df = df_from_csv_file('result_df.csv')
    create_plot(df)
    create_log_plot(df)
    n_vs_N_plot(df)


if __name__ == '__main__':
    main()
